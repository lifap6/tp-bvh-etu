# Accélérations de requêtes de voisinage

Ce TP aborde la géométrie algorithmique, et plus particulièrement les problèmes
de requêtes de voisinages : étant donné un ensemble de points et une requête (un
autre point), déterminer le point de l'ensemble le plus proche de la requête. Le
but est surtout ici de répondre à ce problème lorsque l'ensemble de points ne
varie pas, mais que beaucoup de requêtes de voisinage sont réalisées. Dans ce
cas il est alors possible de commencer par organiser l'ensemble de points pour
pouvoir par la suite réaliser les requêtes plus efficacement.

## Conditions de travail

Ce TP est individuel, à rendre en fin de séance. Votre travail est à rendre sous
la forme d'une archive `.tar.gz` ou `.zip` sur
[TOMUSS](https://tomuss.univ-lyon1.fr). Durant la durée du TP, cous êtes
autorisé·es à consulter vos anciens TP, à naviguer sur internet pour chercher de
la documentation, mais pas à communiquer entre vous. Ce mode d'évaluation repose
sur la confiance et mise sur votre honorabilité plus que sur la surveillance de
vos encadrants. Votre code doit être personnel. Tout morceau de code récupéré
ailleurs doit être annoté comme tel, sans quoi il s'agit d'un plagiat.

## Base de code

Une base de code en C++ vous est fournie pour ce TP. Elle fournit un ensemble
de tests suivant la progression du TP pour tester au fur et à mesure la mise en
place des fonctionnalités demandées. Par défaut aucun test n'est actif, à vous
de les activer lorsque vous avez mis en place la fonctionnalité correspondante.
Ces tests génèrent des images dans le dossier d'exécution au format `svg`.

La base C++ propose un type `Point` qui n'est en réalité qu'un renommage de la
classe `std::array` de la librairie standard. Elle fournit également des outils
pour générer des points et des couleurs aléatoires. Quelques outils de dessins
sont également fournis dans `main.cpp` pour générer les images des tests.
N'hésitez pas à les utiliser pour générer plus d'images pour comprendre des
erreurs éventuelles.

Libre à vous de créer de nouveaux modules ou d'ajouter vos développement à
l'endroit qui vous semble le plus approprié. Vous pouvez modifier tout ce que
vous souhaitez, tant qu'à la sortie, les tests génèrent les images voulues. Vous
pouvez ajouter des méthodes supplémentaires, des constructeurs, des données
membres, selon ce que vous jugerez utile. Attention cependant à faire ne sorte
que les tests soient toujours réalisables.

## Distance entre deux points

Pour pouvoir chercher le point le plus proche d'un autre, il est nécessaire de
pouvoir calculer la distance entre deux points. Géométriquement, la distance
entre deux points $`p_1 = (x_1,y_1)`$ et $`p_2 = (x_2, y_2)`$ est donnée par 
la formule 
$`\mathrm{distance}(p_1, p_2) = \sqrt{(x_2 - x_1)^2 + (y_2 - y_1)^2}`$

#### Contraintes

Dans le fichier `point.cpp`, complétez le code de la fonction `distance`. Vous
pouvez calculer la racine carrée en utilisant la librairie standard :

```cpp
#include <cmath>
```
la fonction s'appelle `sqrt`.

#### Test à activer

Une fois la fonction réalisée, vous pouvez activer le test de la fonction de
distance en décommentant la ligne 
```cpp
#define DISTANCE_TEST
```
en début de fichier. Si votre fonction est correcte, vous devriez obtenir une
image avec des anneaux. Vous devez avoir le même nombre d'anneaux et ils 
doivent avoir tous la même épaisseur.

<center>
<img id="distance-test" src="img/distance.svg", width=500px"></img>
</center>

## Plus proche voisin naif

Pour se donner une base et vérifier l'algorithme plus élaboré qui viendra
ensuite, mettez en place une fonction prenant en paramètre un tableau de
points et une requête et retournant l'indice du point le plus proche dans le
tableau de points. Pas d'algorithme compliqué ici, un simple parcours linéaire
du tableau pour trouver le plus proche suffit.

#### Contraintes

Cette fonction est à implémenter dans le fichier `naive_nearest.cpp` en
respectant le prototype proposé.

#### Test à activer 

Le test est à activer en décommentant la ligne
```cpp
#define NN_TEST
```
Il utilise par défaut un ensemble de 200 points, et réalise 8000 requêtes de
voisinages. Si votre fonction est correcte, vous devriez obtenir joli feu
d'artifice :

<center>
<img id="nn-test" src="img/nn.svg", width=500px"></img>
</center>

Le test dessine pour chaque requête un segment la reliant à son plus proche voisin
parmi les 200 points, et colore les requêtes et les segments de la couleur de
leur point le plus proche.

## Boîtes englobantes alignées sur les axes

Pour accélérer cet algorithme naïf, nous allons utiliser la stratégie des
volumes englobant. L'idée est de regrouper les points par paquets, et de
calculer pour chaque paquet un volume englobant. L'englobant choisi doit être
tel qu'il est facile de calculer la distance entre une requête et un englobant.
Ainsi, si nous avons pour une requête déjà trouvé un point assez proche (le
candidat), nous pouvons comparer la distance entre la requête et l'englobant à
la distance entre la requête et le candidat. Si l'englobant est plus éloigné, il
est alors possible d'ignorer tous les points qu'il contient sans avoir à les
tester un par un.

Pour ce TP nous utiliserons des *boîtes englobantes alignées sur les axes*. Il
s'agit du rectangle minimal contenant tous les points, et dont les côtés sont
verticaux et horizontaux.

### Construction

Étant donné un ensemble de points, la boîte englobante de ces points est 
définie en stockant pour chaque coordonnée le minimum et le maximum des
coordonnées des points qu'elle contient. En nommant `xmin` et `xmax` le minimum
et le maximum le long de l'axe x, et `ymin` et `ymax` les coordonnées le long de
l'axe y, les sommets de la boîte sont aux coordonnées `(xmin, ymin)`, `(xmax,
ymin)`, `(xmax, ymax)`, `(xmin, ymax)`. Il suffit donc de stocker ces quatre
nombres pour définir la boîte.

Votre but ici est d'écrire le nécessaire pour construire la boîte englobante
d'un ensemble de points. Attention, la boîte ne doit pas stocker de points, mais
seulement les bornes du rectangle.

#### Contraintes

Travaillez dans la paire de fichiers `box.hpp` et `box.cpp`. Ils sont pour
l'instant vides, à vous de créer votre classe. Elle doit s'appeler `Box`, et si
`points` est un tableau contenant des points, il doit être possible de 
construire leur boîte englobante via le code suivant :

```cpp
Box box ;
for(int i = 0; i < nb_points; ++i) {
  box.push(points[i]) ;
}

```

De plus pour être compatible avec les tests, il doit être possible d'accéder aux
coordonnées de la boîte en utilisant :

```cpp
box.xmin() ;
box.xmax() ;
box.ymin() ;
box.ymax() ;
```

Ces quatre fonctions doivent retourner le nombre correspondant.

#### Test à activer

Vous pouvez activer le test en décommentant la ligne

```cpp
#define BOX_BUILD_TEST
```

Ce test génère aléatoirement un ensemble de 100 points, puis construit leur
boîte englobante en en créant une boîte vide, et en les poussant un par un. Il
affiche ensuite les points et la boîte pour s'assurer qu'elle les contient tous
et n'est pas trop grande. Si tout se passe bien, vous devriez obtenir l'image
suivante :

<center>
<img id="box-build-test" src="img/box_build.svg", width=500px"></img>
</center>

### Point de la boîte le plus proche d'une requête

Pour pouvoir exploiter nos boîtes englobantes dans les requêtes de voisinage, il
est nécessaire de pouvoir calculer la distance entre un point et une boîte. Si
la boîte est trop loin du point, les points qu'elle contient le sont encore
plus, et n'ont donc pas besoin d'être examinés. Nous allons pour cela calculer
le point de la boîte le plus proche d'un point. Notez qu'il ne s'agit pas de
donner le point le plus proche qui a été poussé dans la boîte, mais de donner
**le point du rectangle le plus proche d'un point**.

Selon la position de la requête, son point le plus proche varie. Si la requête
est dans la boîte, sa distance à la boîte est nulle, et elle est son propre
point le plus proche dans la boîte. Si elle est en dehors de la boîte, le point
le plus proche est un point sur le bord de la boîte. Si $`(x,y)`$ est un point,
et si $`(x',y')`$ est son point le plus proche, alors :

* si $`x < x_{min}`$ alors $`x' = x_{min}`$
* si $`x > x_{max}`$ alors $`x' = x_{max}`$
* si $`x_{min} \leq x \leq x_{max}`$ alors $`x' = x`$

et le même principe fonctionne pour la coordonnée $`y'`$ en fonction de $`y`$.


Votre but pour cette partie est d'ajouter une méthode `nearest` à votre classe 
`Box` pour obtenir le point le plus proche d'une requête. Une fois le point le
plus proche obtenu, il est possible d'utiliser la fonction `distance` pour
calculer la distance à la boîte.

Notez que si vous bloquez sur cette question, vous pouvez avancer aux questions
suivantes, cette fonctionnalités n'est pas nécessaire avant la dernière partie
du TP.

#### Contraintes

Si `p` est un `Point`n il doit être possible de récupérer le point le plus
proche en écrivant

```cpp
Point proche = box.nearest(p) ;
```

#### Test à activer

Le test s'active en décommentant la ligne

```cpp
#define BOX_NEAREST_TEST
```

Il génère une boîte aléatoire pas trop petite, puis calcule le point le plus
proche d'une centaine de requêtes aléatoires. Pour chaque requête, il dessine un
segment reliant la requête à son point le plus proche, ainsi que le point le
plus proche avec une couleur correspondante. Si tout se passe bien, vous devriez
obtenir :

<center>
<img id="box-nearest-test" src="img/box_nearest.svg", width=500px"></img>
</center>

## Hiérarchie de boîtes englobantes

Muni de boîtes englobantes, il faut maintenant se préoccuper de la méthode pour
regrouper les points dans ces boîtes. Les hiérarchies de volumes englobant
généralisent le concept d'arbre binaire de recherche à des données de plus
grande dimension. Dans un arbre binaire de recherche, à chaque nœud, on choisit
une valeur et on répartit d'un côté les valeurs plus petites, et de l'autre les
plus grandes. Ici, nous allons choisir **un axe et une valeur** à chaque nœud,
et répartir les points selon que leur coordonnée sur l'axe est plus grande ou
plus petite que la valeur.

### Premier noeud de l'arbre binaire

Notre hiérarchie de volumes englobants est donc un **arbre binaire** : chaque
nœud de l'arbre a deux enfants. De plus chaque nœud stocke la boîte englobante
des points qu'il englobe. Pour l'instant nous cherchons juste à créer un nœud,
et pas encore d'enfants ou de hiérarchie.

#### Contraintes

Travaillez dans les fichiers `tree.hpp` et `tree.cpp`. Vous y développerez une
classe `Node` pour représenter les nœuds de l'arbre. Pour construire un nœud, il
faudra fournir un `vector` de points ainsi que deux indices `debut` et `fin`
définissant la zone du tableau que le nœud doit contenir. Lors de la
construction, le nœud mettra à jour sa boîte pour englober ses points. Votre
programme devra permettre la création d'un nœud en utilisant la syntaxe

```cpp
std::vector<point> points ;

points.push_back(Point({x, y})) ;
//...

Node n(points, 0, points.size()) ;
```
Il devra également être possible de récupérer la boîte englobante d'un nœud  et
ses enfants via la syntaxe

```cpp
Box b = n.box() ;
const Node* c0 = n.child(0) ; //premier enfant
const Node* c1 = n.child(1) ; //second enfant
```
Notez par contre que le nœud n'a pas besoin de stocker les points qu'il
contient.

#### Test à activer

Le test s'active en décommentant la ligne

```cpp
#define ROOT_NODE_TEST
```

Il génère un ensemble de points dans deux disques, puis construit un nœud sur
la moitié des points, dans le premier disque. Le test devrait donc afficher une
boîte englobant les points de l'un des deux disques :

<center>
<img id="box-nearest-test" src="img/root_node.svg", width=500px"></img>
</center>


### Répartition des points de part et d'autre d'une valeur sur un axe

Pour créer les enfants d'un nœud, il faut donc choisir un axe et une valeur pour
répartir ses points entre les deux enfants. Nous allons écrire une fonction
permettant de réorganiser une portion d'un tableau de points pour regrouper les
points de chaque enfant. Le prototype de cette fonction est donnée dans
`tree.hpp`. Votre travail consiste à en donner une implémentation.

#### Contraintes

Vous devez respecter le prototype de la fonction `partition` proposée dans
`tree.hpp`. Cette fonction prend en paramètre un axe (0 pour x, 1 pour y), et
une valeur. Elle réordonne une plage d'un tableau pour mettre au début les
points de la plage ayant une coordonnée sur l'axe plus petite que la valeur, et
à la fin les points ayant une coordonnée sur l'axe plus grande que la valeur. Le
tableau est également fourni en paramètre à la fonction, ainsi que le début et
la fin de la plage. Par exemple, sur le tableau de points

```
[(3,8), (1,4), (2, 7), (2, 2), (5, 4), (6, 3), (4, 5), (3, 7)]
```

en réordonnant les points sur la plage débutant en 2 (inclus) et finissant en 7
(exclus), selon l'axe 0 avec la valeur 4, le tableau après l'appel pourrait
contenir

```
[(3,8), (1,4), (2, 7), (4, 5), (2, 2), (6, 3), (5, 4), (3, 7)]
```

Ici, les points `(3,8)`, `(1,4)` et `(3,7)` n'ont pas bougé car ils sont en
dehors de la plage modifiée, et les autres points ont été réordonnés. Ici, le
point ayant un x égal à la valeur a été mis au début. La fonction retourne
l'indice du premier point ayant une coordonnée sur l'axe plus grande que la
valeur. Dans l'exemple ci-dessus, elle retourne 5 qui est l'indice de `(6,3)`.

Pour implémenter cette fonction, le plus simple consiste à créer deux `vector`
intermédiaires : un pour points du début, et un pour les points de la fin. 
Vous pouvez alors parcourir linéairement la plage du tableau à modifier, et
recopier les points dans le bon `vector` selon que leur coordonnée est plus
petite ou plus grande que la valeur. Une fois les deux `vector` remplis, dans
uns seconde passe, vous pouvez recopier les points du `vector` des plus petits
dans le `vector` initial en commençant au début de la plage, puis lorsque c'est 
bon, continuer en recopiant les plus grands à la suite, jusqu'à avoir totalement
réécrit la plage. La valeur à retourner est la position dans le tableau où vous
avez commencé à recopier les points plus grands.

Il est possible de coder cette fonction en réalisant un pivot sans créer de
tableau supplémentaire, comme vu en cours pour le Quick Sort. Si vous avez fini
le TP en avance, vous pouvez tenter cette version.

#### Test à activer

Pour tester votre découpe, décommentez la ligne

```cpp
#define POINT_SPLIT_TEST
```

Ce test crée un ensemble de points, puis réordonne le tableau en utilisant votre
fonction `partition` selon l'axe x avec la valeur 0.5. Il récupère la valeur de
retour de votre fonction, puis dessine les points ayant un indice plus petit que
cette valeur de retour en vert, les autres en rouge. La valeur 0.5 de partition
est indiquée par une ligne verticale. Si tout se passe bien, vous devriez avoir
les points verts à gauche de la ligne et les rouges à droite.

<center>
<img id="point-split-test" src="img/point_split.svg", width=500px"></img>
</center>

### Découpe d'un nœud

Pour découper un nœud, il faut choisir l'axe, la valeur, et utiliser votre
`partition` pour réordonner les points. Les enfants peuvent ensuite être créés
sur leurs portions de tableau respectives. Pour choisir l'axe, nous utiliserons
la méthode suivante :

1. calculer la largeur et la hauteur de la boîte
1. choisir l'axe le plus long comme axe de découpe
1. choisir comme valeur de découpe le milieu de cet axe

#### Contraintes

Vous ajouterez une méthode `split` à votre classe nœud. En appelant cette
méthode, un nœud sans enfant déterminera l'axe et la valeur de découpe,
réordonnera la portion du tableau de points contenant les points du nœud, et
créera les enfants sur les deux portions créées. Pour pouvoir réaliser cette
action, votre classe `Node` doit également stocker les indices `debut` et 
`fin` correspondant à la portion du tableau contenant ses points. Votre méthode
prendra en paramètre le tableau de points sur lequel l'arbre est construit.

Ainsi, il sera possible de découper un nœud pour former ses enfants via la
syntaxe

```cpp
n.split(points) ;
```

et vous rendrez accessible la plage du tableau d'un nœud via

```cpp
int b = n.begin() ; //debut de la plage du tableau (inclus)
int e = n.end() ; //fin de la plage du tableau (exclus)
```

#### Test à activer

Pour vérifier la découpe, décommentez la ligne

```cpp
#define NODE_SPLIT_TEST
```

Ce test construit un ensemble de points en trois parties : deux petits disques
et un large rectangle. Il crée ensuite un noeud uniquement sur le rectangle,
puis demande la découpe de ce noeud. La forme du rectangle devrait provoquer une
découpe selon l'axe des x. En sortie, le test affiche l'ensemble des points. Les
points qui sont dans les plages des enfants sont représentés avec une couleur
par enfant, et devraient être encadrés par les boîtes des enfants. Les points
qui ne sont pas touchés par la modification devraient apparaître multicolores en
dehors des boîtes :

<center>
<img id="node-split-test" src="img/node_split.svg", width=500px"></img>
</center>

### Création d'un arbre complet

Une fois la découpe fonctionnelle, il ne reste plus qu'à découper récursivement
les nœuds pour former un arbre. La racine est initialisée pour référencer
l'ensemble des points, puis tant que les nœuds référencent plus d'un certain
nombre de points, ils sont découpés.

Ajoutez une méthode `rec_split `à votre classe `Node` pour réaliser cette découpe 
récursive.

#### Contraintes

Votre méthode prendra en paramètre le tableau de points sur lequel l'arbre est
construit, ainsi que le nombre de points en dessous duquel un nœud n'est plus
découpé. On peut alors créer un arbre complet avec la syntaxe

```cpp
std::vector<Point> points ;

points.push_back(Point({0.5, 0.5})) ;
//...

Node tree(points, 0, points.size()) ;
tree.rec_split(points, 10) ;
```

#### Test à activer

Décommentez la ligne 
```cpp
#define TREE_CREATE_TEST
```

Ce test visualise l'arbre ainsi créé. Il initialise un arbre sur une centaine de
points, appelle la méthode `rec_split` avec par un seuil à 10 points, et enfin
affiche les boîtes englobantes de tous les nœuds de l'arbre créé, ainsi que les
points référencés par les feuilles de l'arbre d'une couleur assortie à celle de
leur feuille. Vous devriez ainsi obtenir une image comme ci-dessous.

<center>
<img id="tree-create-test" src="img/tree_create.svg", width=500px"></img>
</center>

### Plus proche voisin dans un arbre

Vous pouvez désormais exploiter votre arbre pour accélérer vos requêtes de
voisinage. L'idée est de commencer à la racine de l'arbre, et de maintenir un
point candidat pour être le plus proche de la requête, ainsi que sa distance à
la requête. À chaque nœud, si la distance entre la boîte de ses enfants et la
requête est plus petite que la distance du point candidat, l'enfant est visité,
ce qui mettra à jour si besoin le point candidat et sa distance. Pour plus
d'efficacité, il faut visiter d'abord l'enfant le plus proche de la requête.

#### Contraintes

Ajoutez une méthode `nearest` permettant de réaliser la requête de point le plus
proche avec la syntaxe

```cpp
int index = n.nearest(points, query) ;
```
Cette fonction renvoie l'indice dans le tableau points du point le plus proche
de la requête. Pour implémenter cette fonction, il pourra être utile de définit
une méthode auxiliaire qui prend en plus deux paramètres : l'indice du point le
plus proche trouvé jusqu'à présent, et la distance entre ce point et la requête.
L'indice du point le plus proche peut être initialisé avec 0 (ou n'importe quel
autre indice valide de point), et la distance entre le point 0 et la requête.
L'idée de cette fonction est de dire :

1. Si le nœud est une feuille, tester linéairement sur ses points s'il y en a
   un plus proche que celui qu'on a
2. Sinon calculer la distance entre la requête et les boîtes des deux enfants
3. Si l'enfant le plus proche est à une distance plus petite que la distance
   actuelle, l'explorer récursivement
4. Si l'entant le plus éloigné est à une distance plus petite que la distance
   actuelle, l'explorer récursivement
5. Retourner l'indice du point le plus proche

Lors des explorations récursives, l'appel est réalisé avec en paramètre l'indice
du point actuellement le plus proche. Si l'appel retourne un indice différent,
c'est qu'un point encore plus proche a été trouvé, il faut alors mettre à jour
le point le plus proche et la distance correspondante.

#### Test à activer

Assurez-vous ensuite de décommenter les lignes
```cpp
#define NN_TEST
#define TREE_NEAREST_TEST
```

Ce test se rajoute à celui de votre algorithme naïf pour tester votre nouvel
algorithme, et il vérifie que les résultats des deux algorithmes sont
identiques. [L'image produite ne devrait pas changer](#nn-test). Le test mesure
également les temps d'exécution pour vérifier que cette méthode est plus rapide
que la naïve.
