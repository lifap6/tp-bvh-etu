#include "point.hpp"

#include <random>

//-- seed random numbers --

//Note that these functions use a common internal random engine and are
//therefore not thread safe.

static std::default_random_engine alea ;

namespace SamplingOptions {
  void use_random_device() ;
}

//-- 1D sampling --

int rand_index(int begin, int end) ;
double rand_double() ;

//-- 2D sampling --

//random point in the unit square
Point rand_in_square() ;

//random point on the unit circle
Point rand_in_circle() ;

//random point in the unit disk
Point rand_in_disk() ;

//-- Colors --

Color rand_bright_color() ;
