#include "point.hpp"

#include <cmath>

double distance(const Point& p1, const Point& p2) {
  //Q1 : remplacez le contenu de cette fonction
  return 0 ;
}

Point operator+(const Point& p1, const Point& p2) {
  return Point({p1[0] + p2[0], p1[1] + p2[1]}) ;
}

Point operator*(double s, const Point& p1) {
  return Point({s*p1[0], s*p1[1]}) ;
}

std::ostream& operator<<(std::ostream& out, const Point& p) {
  out << "[ " ;
  out << p[0] << " " ;
  out << p[1] << " " ;
  out << "]" ;
  return out ;
}
