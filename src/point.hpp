#ifndef LIFAPC_POINT_HPP
#define LIFAPC_POINT_HPP

#include <array>
#include <iostream>

using Point = std::array<double, 2> ;
using Color = std::array<double, 3> ;

Point operator+(const Point& p1, const Point& p2) ;
Point operator*(double s, const Point& p1) ;

double distance(const Point& p1, const Point& p2) ;

std::ostream& operator<<(std::ostream& out, const Point& v) ;

#endif
