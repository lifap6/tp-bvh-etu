#ifndef LIFAPC_TREE_HPP
#define LIFAPC_TREE_HPP

#include "point.hpp"
#include "box.hpp"

#include <vector>

//Q5 : créez votre structure node ici
//
//elle devra avoir
//
// - un contructeur prenant en paramètre
//   . une référence sur un vector de points
//   . deux int debut et fin pour la plage concernée du vector
// - une méthode child(i)
//   . child(0) -> adresse du premier enfant
//   . child(1) -> adresse du second enfant
// - une méthode box() envoyant la boîte du noeud
// - deux méthodes begin() et end() renvoyant la plage du noeud
//
//elle ne devra pas
//
// - stocker les points qu'elle englobe

//Q7 : ajoutez la méthode split(points) pour découper un noeud en deux enfants

//Q8 : ajoutez la méthode rec_split(points, max_points) pour créer l'arbre

//Q9 : ajoutez la méthode nearest(points, query)


//Q6 : implémentez la fonction suivante pour réordonner un tableau
//  
// - axis est l'axe selon lequel les points sont répartis
// - offset est la valeur sur l'axe pour séparer les points
// - points est le tableau à réordonner
// - begin (inclus) et end (exclus) indique la plage du tableau

int partition(int axis, double offset, std::vector<Point>& points, int begin, int end) ;



#endif
